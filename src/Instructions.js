import React from "react";
import { Paper, Typography } from "@material-ui/core";

class Instructions extends React.Component {
  render() {
    return (
      <Paper elevation={2} style={{ marginLeft: "20px" }}>
        <Typography variant={"h6"}>Markdown allowed:</Typography>
        <Typography paragraph>*bold text*</Typography>
        <Typography paragraph>_italic text_</Typography>
        <Typography paragraph>[inline URL](http://www.example.com/)</Typography>
        <Typography paragraph>`inline fixed-width code`</Typography>
        <Typography variant={"h6"}>HTML allowed:</Typography>
        <Typography paragraph>
          &lt;b&gt;bold&lt;/b&gt;, &lt;strong&gt;bold&lt;/strong&gt;
        </Typography>
        <Typography paragraph>
          &lt;i&gt;italic&lt;/i&gt;, &lt;em&gt;italic&lt;/em&gt;
        </Typography>
        <Typography paragraph>
          &lt;a href=&quot;http://www.example.com/&quot;&gt;inline URL&lt;/a&gt;
        </Typography>
        <Typography paragraph>
          &lt;code&gt;inline fixed-width code&lt;/code&gt;
        </Typography>
        <Typography paragraph>
          &lt;pre&gt;pre-formatted fixed-width code block&lt;/pre&gt;
        </Typography>
        <Typography variant={"h6"}>Silent Message:</Typography>
        <Typography paragraph>
          Sends the message without sound for the receiver
        </Typography>
      </Paper>
    );
  }
}

export default Instructions;
