import React from "react";
import { Paper, Typography } from "@material-ui/core";

class Errors extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (this.props.callFinishedStatus && !this.props.APIDataOK) {
      return (
        <Paper elevation={2} style={{ marginRight: "100px" }}>
          <Typography variant={"body1"} align={"center"} color={"error"}>
            Some error has occurred... Oops!
          </Typography>
        </Paper>
      );
    } else {
      return null;
    }
  }
}

export default Errors;
