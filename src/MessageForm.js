import React from "react";
import LogMessagesOnScreen from "./LogMessagesOnScreen";
import {
  Grid,
  TextField,
  Paper,
  Button,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
  Switch
} from "@material-ui/core";
import Errors from "./Errors";
import SentMessagesCounter from "./SentMessagesCounter";

class MessageForm extends React.Component {
  state = {
    message: "",
    APIData: {},
    callFinishedStatus: false,
    logs: {},
    messageCounter: 0,
    format: "",
    silentMessage: false,
    sendButtonDisabled: false
  };

  handleFormSubmit = event => {
    event.preventDefault();
    this.sendMessage();
  };

  handleMessageInput = event => {
    this.setState({ message: event.target.value });
  };

  handleFormatChange = event => {
    this.setState({ format: event.target.value });
  };

  handleSwitchChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  buttonCooldown = seconds => {
    if (!this.state.sendButtonDisabled && this.state.message !== "") {
      this.setState({ sendButtonDisabled: true });
      setTimeout(() => {
        this.setState({ sendButtonDisabled: false });
      }, seconds);
    }
  };

  resetForm = () => {
    this.setState({
      message: "",
      APIData: {},
      callFinishedStatus: false,
      logs: {},
      messageCounter: 0,
      format: "",
      silentMessage: false
    });
  };

  increaseMessageCounter = () => {
    this.setState(prevState => {
      return {
        messageCounter: prevState.messageCounter + 1
      };
    });
  };

  addNewLog = () => {
    this.setState({
      logs: {
        ...this.state.logs,
        ["log" + Math.floor(Math.random() * 10000)]: {
          message: this.state.message,
          date: this.state.APIData.result.date
        }
      }
    });
  };

  sendMessage = () => {
    fetch(
      `${process.env.TELEGRAM_API}${
        process.env.TELEGRAM_BOT_TOKEN
      }/sendMessage?chat_id=${process.env.TELEGRAM_CHAT_ID}&parse_mode=${
        this.state.format
      }&disable_notification=${this.state.silentMessage}&text=${
        this.state.message
      }`
    )
      .then(response => response.json())
      .then(APIResult => {
        this.setState({
          APIData: APIResult,
          callFinishedStatus: true
        });
        this.addNewLog();
        this.increaseMessageCounter();
      });
  };

  render() {
    return (
      <Grid
        container
        direction={"row"}
        justify={"space-evenly"}
        alignItems={"center"}
      >
        <Grid item sm={6}>
          <Grid
            container
            direction={"column"}
            justify={"space-evenly"}
            alignItems={"center"}
          >
            <Grid item>
              <form onSubmit={this.handleFormSubmit}>
                <Grid item>
                  <TextField
                    id="message"
                    label="Message: "
                    type="textarea"
                    margin="normal"
                    variant="outlined"
                    rowsMax={10}
                    required
                    multiline
                    value={this.state.message}
                    onChange={this.handleMessageInput}
                  />
                </Grid>
                <Grid
                  container
                  direction={"row"}
                  justify={"space-evenly"}
                  alignItems={"center"}
                >
                  <Grid item>
                    <RadioGroup
                      aria-label="Formatting"
                      name="format"
                      value={this.state.format}
                      onChange={this.handleFormatChange}
                    >
                      <FormControlLabel
                        value="HTML"
                        control={<Radio />}
                        label="HTML"
                      />

                      <FormControlLabel
                        value="Markdown"
                        control={<Radio />}
                        label="Markdown"
                      />
                    </RadioGroup>
                  </Grid>
                  <Grid item>
                    <Grid
                      container
                      direction={"column"}
                      justify={"space-evenly"}
                      alignItems={"center"}
                    >
                      <Grid item>
                        <FormControlLabel
                          control={
                            <Switch
                              checked={this.state.silentMessage}
                              onChange={this.handleSwitchChange(
                                "silentMessage"
                              )}
                              value="silentMessage"
                            />
                          }
                          label="Silent Message"
                        />
                      </Grid>
                      <Grid item>
                        <Button
                          style={{ border: "1px solid gray" }}
                          size={"small"}
                          onClick={this.resetForm}
                        >
                          Reset form
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Button
                    variant="outlined"
                    color="primary"
                    size={"large"}
                    type={"submit"}
                    onClick={() => {
                      this.buttonCooldown(3000);
                    }}
                    disabled={this.state.sendButtonDisabled}
                  >
                    Send
                  </Button>
                </Grid>
              </form>
            </Grid>
            <Grid item>
              <SentMessagesCounter messageCounter={this.state.messageCounter} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item sm={6}>
          <LogMessagesOnScreen
            data={this.state.callFinishedStatus ? this.state.logs : false}
          />
          <Errors
            callFinishedStatus={this.state.callFinishedStatus}
            APIDataOK={this.state.APIData.ok}
          />
        </Grid>
      </Grid>
    );
  }
}

export default MessageForm;
