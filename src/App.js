import React from "react";
import { render } from "react-dom";
import MessageForm from "./MessageForm";
import Instructions from "./Instructions";
import Header from "./Header";
import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./Theme";

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Grid
          container
          direction="column"
          justify={"space-evenly"}
          alignItems="center"
        >
          <Grid item>
            <Header />
          </Grid>
          <Grid
            container
            direction={"row"}
            justify={"space-evenly"}
            alignItems={"center"}
          >
            <Grid item sm={3}>
              <Instructions />
            </Grid>
            <Grid item sm={9}>
              <MessageForm />
            </Grid>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    );
  }
}

render(<App />, document.getElementById("root"));
