import React from "react";
import { Grid, Paper, Typography } from "@material-ui/core";

class LogMessagesOnScreen extends React.Component {
  render() {
    function getHumanReadableDate(unix) {
      var dt = new Date(unix * 1000);
      var hr = dt.getHours();
      var m = "0" + dt.getMinutes();
      var s = "0" + dt.getSeconds();
      return " " + hr + ":" + m.substr(-2) + ":" + s.substr(-2);
    }

    if (this.props.data) {
      return (
        <Grid container>
          <Grid item>
            {Object.keys(this.props.data).map(
              function(key) {
                return (
                  <Paper
                    key={key}
                    elevation={2}
                    style={{ marginRight: "20px" }}
                  >
                    <Typography variant="body1" align={"center"}>
                      Message {'"'}
                      <strong>{this.props.data[key].message}</strong>
                      {'" '}
                      was successfully sent to the bot at
                      {getHumanReadableDate(this.props.data[key].date)}
                    </Typography>
                  </Paper>
                );
              }.bind(this)
            )}
          </Grid>
        </Grid>
      );
    } else {
      return (
        <Grid container>
          <Grid item>
            <Paper elevation={2} style={{ marginRight: "20px" }}>
              <Typography variant={"body1"} align={"center"}>
                Logs will appear here as soon as a message is successfully sent.
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      );
    }
  }
}
export default LogMessagesOnScreen;
