import React from "react";
import { Typography } from "@material-ui/core";

class Header extends React.Component {
  render() {
    return (
      <header>
        <Typography variant={"h4"} gutterBottom align={"center"}>
          Send a message to my Telegram Bot
        </Typography>
      </header>
    );
  }
}

export default Header;
