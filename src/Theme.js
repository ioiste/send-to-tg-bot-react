import { createMuiTheme } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";
import blue from "@material-ui/core/colors/blue";
import red from "@material-ui/core/colors/red";

const Theme = createMuiTheme({
  palette: {
    primary: green,
    secondary: blue,
    error: red
  },
  typography: {
    useNextVariants: true,
    fontSize: 12
  },
  overrides: {
    MuiPaper: {
      root: {
        color: "white",
        backgroundColor: "white",
        border: "2px solid #4caf50",
        padding: "15px",
        marginTop: "15px",
        marginBottom: "15px"
      }
    }
  }
});

export default Theme;
