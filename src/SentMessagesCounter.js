import React from "react";
import { Paper, Typography } from "@material-ui/core";

class SentMessagesCounter extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (this.props.messageCounter) {
      return (
        <Paper elevation={2} style={{ marginRight: "100px" }}>
          <Typography variant={"body1"} align={"center"}>
            Messages Sent: {this.props.messageCounter}
          </Typography>
        </Paper>
      );
    } else {
      return null;
    }
  }
}

export default SentMessagesCounter;
